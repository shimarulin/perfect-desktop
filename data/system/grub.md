# GRUB

## Fix grub menu screen

```
sudo apt-get install v86d hwinfo
sudo hwinfo --framebuffer
```

Edit `etc/default/grub`:
```
# The resolution used on graphical terminal
# note that you can use only modes which your graphic card supports via VBE
# you can see them in real GRUB with the command `vbeinfo'
# GRUB_GFXMODE=1920x1080x24
GRUB_GFXMODE=1280x800x24
GRUB_GFXPAYLOAD_LINUX=keep
```



This is the solution that worked for me. (for those who have this common issue)

Run in Terminal
```
sudo apt-get install v86d
sudo gedit /etc/default/grub
```
Replace Line: GRUB_CMDLINE_LINUX_DEFAULT="quiet splash". 
New Line: GRUB_CMDLINE_LINUX_DEFAULT="quiet splash nomodeset video=uvesafb:mode_option=1920x1080-24,mtrr=3,scroll=ywrap"

Replace Line: #GRUB_GFXMODE=640x480.
New Line: #GRUB_GFXMODE=1920x1080

Save & Close

```
sudo gedit /etc/initramfs-tools/modules
```

Add Line: uvesafb mode_option=1920x1080-24 mtrr=3 scroll=ywrap

Save & Close

```
sudo update-grub2
sudo update-initramfs -u
```

Warning: /sbin/fsck.btrfs doesn't exist, can't install to initramfs, ignoring.


sudo nano /etc/initramfs-tools/conf.d/splash

And fill it with the following line

echo FRAMEBUFFER=y

Now run the following command

$ sudo update-initramfs -u

