# Benchmark tools

# bootchart

С версии 15.04 bootchart встроен в systemd. Для включения логгирования нужно добавить в GRUB опцию загрузки ядра init=/lib/systemd/systemd-bootchart (`GRUB_CMDLINE_LINUX_DEFAULT="quiet splash"`) в файле `/etc/default/grub` и применить изменения командой `sudo update-grub`.

