# Система

Поставил убунту на раздел с btrfs со сжатием lzo.
Прописал сжатие в fstab.
swap на отдельном разделе.

добавил системный монитор в автозагрузку.
при старте 400мб, подкачка не используется.

включил bootchart в файле `/etc/default/grub`.
последние операции чтения-записи - в диапазоне 22-23 сек

сравнил btrfs с ext4 - данных диск пишется больше и чаще. Последние операции чтения-записи зафиксированы позже 25 сек.

в 15.10 zswap по умолчанию включен (dmesg| grep -i zswap). 

так как на btrfs нельзя использовать swap-файл, советуют создать его как loop-устройство. Однако это приводит к снижению производительности. При установленном параметре vm.swappiness=1 накладные расходы привели к полному зависанию тестовой системы. На аналогичном тесте для swap в отдельном разделе на HDD наблюдалось сильное снижение отзывчивости системы в течение 5-6 секунд, после чего работа нормализовалась. При установленном vm.swappiness=10 снижение отзывчивости наблюдалось в течение 1-2 секунд за счет более ранней записи и как следствие, снижения объема данных, необходимых для единовременной записи.

приоритет swap - чем больше число, тем выше приоритет

systemd.swap
 - http://www.freedesktop.org/software/systemd/man/systemd.swap.html
 - https://github.com/Nefelim4ag/systemd-swap

snapshot
 - https://github.com/openSUSE/snapper
 - http://snapper.io/documentation.html
 - https://poisonpacket.wordpress.com/2015/06/06/apt-btrfs-snapper-apt-get-snapper-snapshots/
 - https://poisonpacket.wordpress.com/2015/06/04/installing-btrfs-snapper-gui-on-ubuntu-and-ubuntu-derivatives/
 - https://github.com/agronick/apt-btrfs-snapper
 - https://github.com/ricardomv/snapper-gui
 - https://wiki.archlinux.org/index.php/Btrfs_-_Tips_and_tricks
 - https://wiki.archlinux.org/index.php/Snapper


