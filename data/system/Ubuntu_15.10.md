# Ubuntu 15.10. Установка на SSD + HDD

Для установки системы на SSD я выбрал файловую систему btrfs со сжатием lzo. Она хорошо зарекомендовала себя в тестах в качестве файловой системы для корневого раздела, однако данные я бы все же предпочел хранить на HDD с ext4. Однако я не стал монтировать раздел на естком диске в качестве `/home`, как советуют в интернете, чтобы снизить нагрузку на SSD. Из этого раздела читаются пользовательские конфигурационные файлы, и очень странно, на мой взгляд, покупать SSD и не использовать его по назначению, ведь он покупается обычно для увеличения производительности дисковой системы.

## Установка с LiveUSB на btrfs со сжатием

Так как сжатие в btrfs включается на этапе монтирования, нам придется добавить эту опцию вручную. 

```
sudo -i
mv /bin/mount /bin/mount.bin
gedit /bin/mount
```

Файл `/bin/mount`:
```
#!/bin/sh
if echo $@ | grep "btrfs" >/dev/null; then 
   /bin/mount.bin $@ -o compress=lzo 
else 
   /bin/mount.bin $@ 
fi
```

Далаем файл исполняемым:
```
chmod 755 /bin/mount
```

Дальше процедура установки стандартна, за исключением того, что выбирается разметка диска вручную. 

Несмотря на достаточный объем оперативной памяти, я создал отдельный swap-раздел на SSD. И опять я отступил от популярных советов создать swap на HDD или не создавать вовсе. Никогда не знаешь, когда закончится оперативка, и если это случится, я бы не хотел проводить минуты томительного ожидания, ожидая реакции от системы. Почему не файл, а именно раздел? Из ограничений размещения swap в файле стоит упомянуть, что на файловой системе btrfs swap-файл разместить нельзя. Но можно создать его как loop-устройство. Однако это приводит к снижению производительности, причем довольно существенному, судя по поведению тестовой системы, на которой предварительно производились замеры. 

Еще одна причина использовать swap - использование zswap, который занимается сжатием страниц памяти. В Ubuntu 15.10 zswap включен по умолчанию, но при отсутствии swap он работать не будет. Наличие zswap позволяет снизить накладные расходы на используемую память на 20-40%. В теории до 50%, но на практике далеко не все сраницы хорошо сжимаются. Парадоксально, но отключая swap, можно значительно приблизить момент, когда он будет крайне необходим.

Для точки монтирования `/boot` также был создан отдельный раздел, чтобы иметь возможность восстановления системы, либо запуска резервной системы при крахе основной. Лучше быть застрахованным от подобных неприятностей.

Большая часть диска естественно была отдана под систему. Можно не заботиться о разбиении диска на разделы, более эффективно использовать подтома btrfs. По умолчанию будут созданы подтома для корня системы и точки `/home`. 

Я также сразу добавил точку монтирования `/media/data` для раздела HDD с данными.

После установки загружаемся в установленную систему и продолжаем настройку. 

## Включаем Bootchart

Для любителей проанализировать ход загрузки системы по красивым графикам, можно задействовать bootchart, который уже присутствует в системе. Делается это в `/etc/default/grub`:
```
GRUB_CMDLINE_LINUX_DEFAULT="quiet splash init=/lib/systemd/systemd-bootchart"
```

Не забываем после внесения изменений обновить конфигурацию GRUB:
```
sudo update-grub
```

Желательно bootchart конечно потом отключить, когда он не будет нужен.

## Параметры VM

### SWAP

/etc/sysctl.conf
```
vw.swappiness=1
```

#### Disc cache
```
sync
echo 3 > /proc/sys/vm/drop_caches
dd if=/dev/zero of=/tmp/testfile count=1 bs=900M

sysctl -w vm.vfs_cache_pressure=100
find / > /dev/null
cp /tmp/testfile /tmp/testfile2
time find / > /dev/null

sysctl -w vm.vfs_cache_pressure=50
find /  > /dev/null
cp /tmp/testfile2 /tmp/testfile3
time find / > /dev/null

rm -f /tmp/testfile /tmp/testfile2 /tmp/testfile3
```

/etc/sysctl.conf
```
vm.vfs_cache_pressure=100
```

### Монтирование дисковых подсистем

#### Опции монтирования

##### BTRFS

commit='seconds'
    (since: 3.12, default: 30)
    Set the interval of periodic commit. Higher values defer data being synced to permanent storage with obvious consequences when the system crashes. The upper bound is not forced, but a warning is printed if it’s more than 300 seconds (5 minutes). 

compress='type'
    Control BTRFS file data compression. Type may be specified as zlib, lzo or no (for no compression, used for remounting). If no type is specified, zlib is used. If compress-force is specified, all files will be compressed, whether or not they compress well. NOTE: If compression is enabled, nodatacow and nodatasum are disabled. 

```
compress=lzo,commit=300,ssd
```

#### Монтирование `/tmp` в `tmpfs`

/etc/fstab
```
tmpfs /tmp tmpfs defaults,noatime,nosuid,nodev,exec 0 0
```

### Установка ACL прав доступа к `/opt`

```
sudo ./add-permissions.sh /opt <username>
```

### btrfs-tools и initramfs

Если выполнить `sudo update-initramfs -u` увидим следующее сообщение:
```
Warning: /sbin/fsck.btrfs doesn't exist, can't install to initramfs, ignoring.
```

Это просходит потому, что fsck.btrfs на самом деле лежит в каталоге `/bin`. Исправим ситуацию, создав символическую ссылку на бинарник:
```
sudo ln -s /bin/fsck.btrfs /sbin/
```

Подробности: [Bug #1483975 “update-initramfs complains about missing /sbin/fsc...”](https://bugs.launchpad.net/ubuntu/+source/initramfs-tools/+bug/1483975)

