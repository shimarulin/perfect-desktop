# Gnome

## Каталоги по умолчанию

Чтобы хранить свои пользовательские данные на отдельном HDD диске, но в то же время разместить свой домашний каталог на SSD, я сделал в домашнем каталоге символические ссылки на каталоги в разделе на HDD. Раздел монтируется в `/media/data` и содержит каталоги пользователей.

```
# User directories
ln -s /media/data/shimarulin/desktop "$HOME/Рабочий стол"
ln -s /media/data/shimarulin/documents "$HOME/Документы"
ln -s /media/data/shimarulin/inbox "$HOME/Входящие"
ln -s /media/data/shimarulin/music "$HOME/Музыка"
ln -s /media/data/shimarulin/pictures "$HOME/Изображения"
ln -s /media/data/shimarulin/shared "$HOME/Общедоступные"
ln -s /media/data/shimarulin/templates "$HOME/Шаблоны"
ln -s /media/data/shimarulin/videos "$HOME/Видео"
```

Опционально можно сразу создать дополнительные, необязательные ссылки:
```
# User data root
ln -s /media/data/shimarulin ~/.user-data
# Extra directories
ln -s /media/data/shimarulin/projects "$HOME/Проекты"
```

Каталоги по умолчанию определены в файле `~/.config/user-dirs.dirs`. Чтобы изменить их просто отредактируйте соответствующие поля. Например, я изменил `XDG_DOWNLOAD_DIR="$HOME/Загрузки"` на `XDG_DOWNLOAD_DIR="$HOME/Входящие"`.

browser-plugin-freshplayer-pepperflash


## Приложения Gnome

### Gedit

#### Дополнительные пакеты

```
sudo apt-get install gedit-plugins
```

### GPaste

сборка из сырцов

требуются

valac gobject-introspection intltool libappstream-glib-dev gnome-control-center-dev libgirepository1.0-dev libclutter-1.0-dev

### Apps
gnome-calendar
gnome-boxes
gnome-clocks
gnome-builder
gnome-schedule (cron)
gnome-specimen (fonts)

http://gnumdk.github.io/lollypop-web/
http://vocalproject.net/



