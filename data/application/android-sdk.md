# Android SDK

## Настройка переменных окружения

Создадим в `/etc/profile.d/` новый файл `android-sdk-linux.sh` следующего содержания:

```
export ANDROID_HOME=/opt/android-sdk-linux
export PATH=$PATH:/opt/android-sdk-linux/tools:/opt/android-sdk-linux/platform-tools
```

 - [EnvironmentVariables - Community Help Wiki](https://help.ubuntu.com/community/EnvironmentVariables#A.2Fetc.2Fprofile.d.2F.2A.sh)






/etc/profile.d/*.sh

Files with the .sh extension in the /etc/profile.d directory get executed whenever a bash login shell is entered (e.g. when logging in from the console or over ssh), as well as by the DisplayManager when the desktop session loads.

You can for instance create the file /etc/profile.d/myenvvars.sh and set variables like this:

export JAVA_HOME=/usr/lib/jvm/jdk1.7.0
export PATH=$PATH:$JAVA_HOME/bin


## Troubleshooting Ubuntu

 - If you need help installing and configuring Java on your development machine, you might find these resources helpful:
     - https://help.ubuntu.com/community/Java
     - https://help.ubuntu.com/community/JavaInstallation
 - Here are the steps to install Java:

    1. If you are running a 64-bit distribution on your development machine, you need to install additional packages first. For Ubuntu 13.10 (Saucy Salamander) and above, install the libncurses5:i386, libstdc++6:i386, and zlib1g:i386 packages using apt-get:

    ```
    sudo dpkg --add-architecture i386
    sudo apt-get update
    sudo apt-get install libncurses5:i386 libstdc++6:i386 zlib1g:i386 
    lib32ncurses5 lib32z1  
    libgl1-mesa-dev:i386
    apt-get install libstdc++6:i386 lib32z1 lib32ncurses5 libbz2-1.0
    export PATH=/opt/android-sdk-linux/tools:/opt/android-sdk-linux/platform-tools:$PATH
    ```

    For earlier versions of Ubuntu, install the ia32-libs package using apt-get:
        
    ```
    apt-get install ia32-libs
    ```


    2. Next, install Java:
        
    ```
    apt-get install sun-java6-jdk
    ```

## Links
 - http://developer.android.com/sdk/installing/index.html?pkg=tools
 - http://tutorialforlinux.com/2015/10/16/how-to-install-android-sdk-tools-only-for-lubuntu-15-10-wily-32-64bit-linux-visual-guide/

