# Git

## Настройка git

### Настройки для пользователя системы

```
git config --global user.name "Vyacheslav Shimarulin"
git config --global user.email shimarulin@gmail.com
git config --global color.ui true
git config --global core.autocrlf input
git config --global push.default simple
```

warning: push.default не установлен; его неявное значение было изменено в Git версии 2.0 с «matching» на «simple». Чтобы прекратить вывод этого сообщения и сохранить старое поведение, используйте:

  git config --global push.default matching

Чтобы прекратить вывод этого сообщения и использовать новое поведение, используйте:

  git config --global push.default simple

Когда push.default установлено в «matching», git будет отправлять изменения локальных веток в существующие внешние ветки с таким же именем.

Начиная с Git версии 2.0, по умолчанию используется более консервативное поведение «simple», которое отправляет изменения текущей ветки в соответствующую внешнюю ветку, из которой «git pull» забирает изменения.

Смотрите «git help config» и ищите «push.default» для дополнительной информации.
(режим «simple» появился в Git версии 1.7.11. Используйте похожий режим «current» вместо «simple», если вы иногда используете старые версии Git)

