#Gnome
## Upgrade to 3.16 in Ubuntu 15.04

http://www.omgubuntu.co.uk/2015/06/how-to-upgrade-to-gnome-3-16-in-ubuntu-15-04

```
#!bash
    
    sudo add-apt-repository ppa:gnome3-team/gnome3-staging
    sudo add-apt-repository ppa:gnome3-team/gnome3
    sudo apt-get update && sudo apt-get install gnome-shell gnome-session
    
```

## Utilites
### Alacarte
Application menu editor (or [MenuLibre](https://smdavis.us/projects/menulibre/) - without Gnome as dependency)

## Application integration
### Firefox

[Firefox GNOME Tutorial](https://github.com/chpii/Headerbar)

- [GNOME 3](https://addons.mozilla.org/ru/firefox/addon/adwaita/) - GNOME 3 theme for Firefox — Previously known as "Adwaita"
- [GNOME Theme Tweak](https://addons.mozilla.org/ru/firefox/addon/gnome-theme-tweak/) - Extension for customizing GNOME theme.
- [GNotifier](https://addons.mozilla.org/ru/firefox/addon/gnotifier/)
- [HTitle](https://addons.mozilla.org/ru/firefox/addon/htitle/) - Скрывает заголовок у развернутого на весь экран окна (в стиле приложений GNOME 3).


# KDE
## DSP
### Dependencies

    build-essential
    cmake
    kdelibs5-dev
    kwin-dev
    libkf5config-dev
    extra-cmake-modules
    libkf5kdelibs4support-dev
    libqt5x11extras5-dev
    libkdecorations2-dev
    libkf5windowsystem-dev
    libxcb-image0-dev

### XCB patch

Make error

```
#!bash

    In file included from /usr/include/xcb/xproto.h:15:0,
                     from /home/svm/Projects/styleproject-code/stylelib/xhandler.cpp:11:
    /usr/include/xcb/xcb.h:401:7: error: ‘xcb_query_extension_reply_t’ does not name a type
     const xcb_query_extension_reply_t *xcb_get_extension_data(xcb_connection_t *c, xcb_extension_t *ext);
           ^
    /usr/include/xcb/xcb.h:437:7: error: ‘xcb_setup_t’ does not name a type
     const xcb_setup_t *xcb_get_setup(xcb_connection_t *c);
```

/usr/include/xcb/xcb.h:

    401 -const xcb_query_extension_reply_t *xcb_get_extension_data(xcb_connection_t *c, xcb_extension_t *ext);
    401 +const struct xcb_query_extension_reply_t *xcb_get_extension_data(xcb_connection_t *c, xcb_extension_t *ext);
    ...
    437 -const xcb_setup_t *xcb_get_setup(xcb_connection_t *c);
    437 +const struct xcb_setup_t *xcb_get_setup(xcb_connection_t *c);


### Notes
#### http://hombremaledicto.deviantart.com/art/Minimal-514444700

DSP has multi-presets support, you can start from the existing config file :

    cp ~/.config/dsp/dsp.conf ~/.config/dsp/another.conf

Edit ~/.config/dsp/dsp.conf and append these lines to enable another.conf
for any Qt4 app you want (using binary names):

    [Presets]
    another=gwenview, ksysguard, vlc, smplayer

Then you can edit ~/.config/dsp/another.conf, eventually you can bind it
to a color palette (from the current kde colorscheme, or by selecting another one)
and appending the line

    palette=mypalette

Under the \[General\] section of the file. This will create a file "mypalette.conf" in dsp config folder.
Then the preset "another.conf" will be bound to this colorscheme, even if you change
the global one from kde system settings  :)

You can create and use the number of presets you want, as long as you bind different apps :P

And of course, always refer to the wiki on how to edit config files: sourceforge.net/p/styleproject…

Hope i made it crystal clear, but if you have questions, just ask ;)

#### http://hombremaledicto.deviantart.com/art/Out-of-date-503677006
Ok, i've talked with the dev and it's ok for him:

    git clone git://git.code.sf.net/p/styleproject/code styleproject-code
    cd styleproject-code
    mkdir build && cd build
    cmake ../ -DCMAKE_INSTALL_PREFIX=/usr; make && sudo make install

If compilation went fine you can then run a test, for example:

    dolphin -style styleproject

Finally:

    kcmshell4 style kwindecoration

And select styleproject as default style & deco ;)

There's no GUI for configuration at the moment, you'll have to create a config file
and edit the settings manually, with the help of the wiki :

    mkdir ~/.config/dsp
    cd ~/.config/dsp && touch dsp.conf

The file dsp.conf has effect on every app, if the style is settled as the default,
but you can eventually create presets for individual apps by putting a config file
with the application binary name within ~/.config/dsp (e.g. dolphin.conf).